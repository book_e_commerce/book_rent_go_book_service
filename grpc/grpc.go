package grpc

import (
	"book_e_commerce/book_rent_go_book_service/config"
	"book_e_commerce/book_rent_go_book_service/genproto/book_service"
	"book_e_commerce/book_rent_go_book_service/grpc/client"
	"book_e_commerce/book_rent_go_book_service/grpc/service"
	"book_e_commerce/book_rent_go_book_service/pkg/logger"
	"book_e_commerce/book_rent_go_book_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	book_service.RegisterBookServiceServer(grpcServer, service.NewBookService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
