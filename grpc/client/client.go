package client

import (
	"book_e_commerce/book_rent_go_book_service/config"
)

type ServiceManagerI interface{}

type grpcClients struct{}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	return &grpcClients{}, nil
}
