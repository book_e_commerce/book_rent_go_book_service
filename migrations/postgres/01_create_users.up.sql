CREATE TABLE "book" (
    "id" UUID PRIMARY KEY,
    "user_id" UUID NOT NULL, 
    "name" VARCHAR(50) NOT NULL,
    "price" NUMERIC NOT NULL,
    "author_name" VARCHAR(50) NOT NULL,
    "photo_url" VARCHAR NOT NULL,
    "description" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" TIMESTAMP
);








