package postgres

import (
	"book_e_commerce/book_rent_go_book_service/genproto/book_service"
	"book_e_commerce/book_rent_go_book_service/pkg/helper"
	"book_e_commerce/book_rent_go_book_service/storage"
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BookRepo struct {
	db *pgxpool.Pool
}

func NewBookRepo(db *pgxpool.Pool) storage.BookRepoI {
	return &BookRepo{
		db: db,
	}
}

func (c *BookRepo) Create(ctx context.Context, req *book_service.CreateBook) (resp *book_service.BookPrimaryKey, err error) {

	var id = uuid.New().String()

	query := `INSERT INTO "book" (
				id,
				user_id,
				name,
				price,
				author_name,
				photo_url,
				description,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7 now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.UserId,
		req.Name,
		req.Price,
		req.AuthorName,
		req.PhotoUrl,
		req.Description,
	)

	if err != nil {
		return nil, err
	}

	return &book_service.BookPrimaryKey{Id: id}, nil
}

func (c *BookRepo) GetById(ctx context.Context, req *book_service.BookPrimaryKey) (resp *book_service.Book, err error) {

	query := `
			SELECT
				id,
				user_id,
				name,
				price,
				author_name,
				photo_url,
				description,
				created_at,
				updated_at
			FROM "book"
			WHERE deleted_at is null and id = $1
	`

	var (
		id          sql.NullString
		userId      sql.NullString
		name        sql.NullString
		price       sql.NullFloat64
		authorName  sql.NullString
		photoUrl    sql.NullString
		description sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&userId,
		&name,
		&price,
		&authorName,
		&photoUrl,
		&description,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &book_service.Book{
		Id:         id.String,
		UserId:     userId.String,
		Name:       name.String,
		Price:      float32(price.Float64),
		AuthorName: authorName.String,
		PhotoUrl:   photoUrl.String,
		Descripton: description.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *BookRepo) GetAll(ctx context.Context, req *book_service.GetListBookRequest) (resp *book_service.GetListBookResponse, err error) {

	resp = &book_service.GetListBookResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
	)

	query = `
			SELECT
				COUNT(*) OVER(),
				id,
				user_id,
				name,
				price,
				author_name,
				photo_url,
				description,
				created_at,
				updated_at
			FROM "book" as b
			JOIN "author" as a on a.id = b.author_id
			WHERE b.deleted_at is null
		`

	if req.GetSearch() != "" {
		query += " ILIKE '%" + req.Search + "%'"
	}

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += offset + limit

	query, args := helper.ReplaceQueryParams(query, params)

	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			userId      sql.NullString
			name        sql.NullString
			price       sql.NullFloat64
			authorName  sql.NullString
			photoUrl    sql.NullString
			description sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&userId,
			&name,
			&price,
			&authorName,
			&photoUrl,
			&description,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.BookList = append(resp.BookList, &book_service.Book{
			Id:         id.String,
			UserId:     userId.String,
			Name:       name.String,
			Price:      float32(price.Float64),
			AuthorName: authorName.String,
			PhotoUrl:   photoUrl.String,
			Descripton: description.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})

	}

	return
}

func (c *BookRepo) Update(ctx context.Context, req *book_service.UpdateBook) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "book"
			SET
				name = :name,
				author_name = :author_name
				price = :price,
				photo_url = :photo_url
				description = :description
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"name":        req.GetName(),
		"author_name": req.GetAuthorName(),
		"price":       req.GetPrice(),
		"photo_url":   req.GetPhotoUrl(),
		"description": req.GetDescription(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BookRepo) Delete(ctx context.Context, req *book_service.BookPrimaryKey) error {

	query := ` UPDATE "book"
		 SET 
			deleted_at = now()
		 WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
